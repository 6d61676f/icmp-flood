#ifndef PING_H
#define PING_H
#include <algorithm>
#include <arpa/inet.h>
#include <chrono>
#include <cstdbool>
#include <cstring>
#include <ifaddrs.h>
#include <iostream>
#include <net/if.h>
#include <netinet/ip.h>
#include <netinet/ip_icmp.h>
#include <random>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <thread>
#include <unistd.h>
#include <vector>

class Ping
{
public:
  Ping(const char ipSrc[16], const char ipDst[16]);
  Ping();
  virtual ~Ping();
  bool setIp(const char ipSrc[16] = "0xFF", const char ipDst[16] = "0xFF");
  void Flood(uint32_t miliseconds);
  static void flood(int nr_milisecunde = 1000, int nr_fire = 10,
                    const char ipSrc[16] = "0xFF",
                    const char ipDst[16] = "0xFF");

private:
  bool send();
  uint16_t checksum(uint8_t* ptr, size_t bytes);
  int socketFD;
  int packetSize;
  struct sockaddr_in sock;
  uint8_t* packet;
  struct iphdr* ipHeader;
  struct icmphdr* icmpHeader;
  char ip_src[16];
  char ip_dst[16];
  void initHeaders();
  in_addr getDefaultBroadcast();
  bool srcOK = false;
  bool dstOK = false;
};

#endif /* PING_H */
