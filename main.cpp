#include "Ping.h"

// http://www.binarytides.com/icmp-ping-flood-code-sockets-c-linux/

int
main(int argc, char** argv)
{
  try {

    if (argc != 3) {
      throw "nume_aplicatie ip-sursa ip-destinatie";
    }

    Ping p;
    p.setIp(argv[1], argv[2]);
    for (uint64_t i = 0; i < 65535; i++) {
      p.Flood(1000);
      std::this_thread::sleep_for(std::chrono::milliseconds(500));
    }

  } catch (const char* err) {
    std::cerr << err << std::endl;
    return EXIT_FAILURE;
  }
  return EXIT_SUCCESS;
}
