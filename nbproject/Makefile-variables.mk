#
# Generated - do not edit!
#
# NOCDDL
#
CND_BASEDIR=`pwd`
CND_BUILDDIR=build
CND_DISTDIR=dist
# Debug configuration
CND_PLATFORM_Debug=GNU-Linux
CND_ARTIFACT_DIR_Debug=dist/Debug/GNU-Linux
CND_ARTIFACT_NAME_Debug=icmp_flood
CND_ARTIFACT_PATH_Debug=dist/Debug/GNU-Linux/icmp_flood
CND_PACKAGE_DIR_Debug=dist/Debug/GNU-Linux/package
CND_PACKAGE_NAME_Debug=icmpflood.tar
CND_PACKAGE_PATH_Debug=dist/Debug/GNU-Linux/package/icmpflood.tar
# Release configuration
CND_PLATFORM_Release=CLang-Linux
CND_ARTIFACT_DIR_Release=dist/Release/CLang-Linux
CND_ARTIFACT_NAME_Release=icmp_flood
CND_ARTIFACT_PATH_Release=dist/Release/CLang-Linux/icmp_flood
CND_PACKAGE_DIR_Release=dist/Release/CLang-Linux/package
CND_PACKAGE_NAME_Release=icmpflood.tar
CND_PACKAGE_PATH_Release=dist/Release/CLang-Linux/package/icmpflood.tar
#
# include compiler specific variables
#
# dmake command
ROOT:sh = test -f nbproject/private/Makefile-variables.mk || \
	(mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk)
#
# gmake command
.PHONY: $(shell test -f nbproject/private/Makefile-variables.mk || (mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk))
#
include nbproject/private/Makefile-variables.mk
